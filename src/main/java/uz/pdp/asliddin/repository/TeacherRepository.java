package uz.pdp.asliddin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.asliddin.entity.Teacher;

import java.util.UUID;

public interface TeacherRepository extends JpaRepository<Teacher, UUID> {
}
